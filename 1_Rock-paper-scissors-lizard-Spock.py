
#Url for test http://www.codeskulptor.org/

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

# helper functions
import random

def name_to_number(name):
    number = 0
    if (name =="rock"):
        number = 0
    elif (name =="Spock"):
        number = 1
    elif (name =="paper"):
        number = 2
    elif (name =="lizard"):
        number = 3
    elif (name =="scissors"):
        number = 4
    else:
        print 'Error of Conversion name to number'        
    return number

def number_to_name(number):   
    name = ""
    if (number == 0):
        name = "rock"
    elif (number ==1):
        name = "Spock"
    elif (number ==2):
        name = "paper"
    elif (number ==3):
        name = "lizard"
    elif (number ==4):
        name = "scissors"
    else:
        print 'Error of Conversion number to string'
    return name
    

def rpsls(player_choice):    
    
    print "\nPlayer chooses "+ player_choice
    player_number = name_to_number(player_choice)
    comp_number = random.randrange(0,5)
    comp_choice = number_to_name(comp_number)
    print "Computer chooses " + comp_choice 
    diff = (comp_number - player_number)  % 5
    if (comp_number == player_number):
        print "Player and computer tie!"
    elif (diff == 1 or diff == 2):
        print "Computer wins!"
    else:
        print "Player wins!"
    
# test your code - THESE CALLS MUST BE PRESENT IN YOUR SUBMITTED CODE
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

               
# always remember to check your completed program against the grading rubric